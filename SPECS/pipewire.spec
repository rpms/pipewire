%global apiversion   0.3
%global spaversion   0.2

#global snap       20141103
#global gitrel     327
#global gitcommit  aec811798cd883a454b9b5cd82c77831906bbd2d
#global shortcommit %(c=%{gitcommit}; echo ${c:0:5})

# https://bugzilla.redhat.com/983606
%global _hardened_build 1

# where/how to apply multilib hacks
%global multilib_archs x86_64 %{ix86} ppc64 ppc s390x s390 sparc64 sparcv9 ppc64le

%global enable_alsa 1

%if 0%{?fedora}
%global enable_jack 1
%global enable_pulse 1
%global enable_vulkan 1
%endif

# libpulse and libjack subpackages shouldn't have library provides
# as the files they ship are not in the linker path. We also have
# to exclude requires or else the subpackages wind up requiring the
# libs they're no longer providing
# FIXME: the jack-audio-connection-kit and pulseaudio subpackages
# should get the auto-generated Provides: instead, but they do not,
# either with or without the lines below, not sure how to fix that
%global __provides_exclude_from ^%{_libdir}/pipewire-%{apiversion}/.*$
%global __requires_exclude_from ^%{_libdir}/pipewire-%{apiversion}/.*$

Name:           pipewire
Summary:        Media Sharing Server
Version:        0.3.6
Release:        1%{?snap:.%{snap}git%{shortcommit}}%{?dist}
License:        MIT
URL:            https://pipewire.org/
%if 0%{?gitrel}
# git clone git://anongit.freedesktop.org/gstreamer/pipewire
# cd pipewire; git reset --hard %{gitcommit}; ./autogen.sh; make; make distcheck
Source0:        pipewire-%{version}-%{gitrel}-g%{shortcommit}.tar.gz
%else
Source0:	https://gitlab.freedesktop.org/pipewire/pipewire/-/archive/%{version}/pipewire-%{version}.tar.gz
%endif

## upstream patches

## upstreamable patches

## fedora patches
Patch0:		0001-conf-disable-bluez5.patch

BuildRequires:  meson >= 0.49.0
BuildRequires:  gcc
BuildRequires:  pkgconfig
BuildRequires:  pkgconfig(libudev)
BuildRequires:  pkgconfig(dbus-1)
BuildRequires:  pkgconfig(glib-2.0) >= 2.32
BuildRequires:  pkgconfig(gio-unix-2.0) >= 2.32
BuildRequires:  pkgconfig(gstreamer-1.0) >= 1.10.0
BuildRequires:  pkgconfig(gstreamer-base-1.0) >= 1.10.0
BuildRequires:  pkgconfig(gstreamer-plugins-base-1.0) >= 1.10.0
BuildRequires:  pkgconfig(gstreamer-net-1.0) >= 1.10.0
BuildRequires:  pkgconfig(gstreamer-allocators-1.0) >= 1.10.0
%if 0%{?enable_vulkan}
BuildRequires:  pkgconfig(vulkan)
%endif
BuildRequires:  pkgconfig(bluez)
BuildRequires:  systemd-devel >= 184
BuildRequires:  alsa-lib-devel
BuildRequires:  libv4l-devel
BuildRequires:  doxygen
BuildRequires:  xmltoman
BuildRequires:  graphviz
BuildRequires:  sbc-devel
BuildRequires:  libsndfile-devel

Requires(pre):  shadow-utils
Requires:       %{name}-libs%{?_isa} = %{version}-%{release}
Requires:       systemd >= 184
Requires:       rtkit

# https://bugzilla.redhat.com/983606
%global _hardened_build 1

## enable systemd activation
%global systemd 1

%description
PipeWire is a multimedia server for Linux and other Unix like operating
systems.

%package libs
Summary:        Libraries for PipeWire clients
License:        MIT
Recommends:     %{name}%{?_isa} = %{version}-%{release}

%description libs
This package contains the runtime libraries for any application that wishes
to interface with a PipeWire media server.

%package gstreamer
Summary:        GStreamer elements for PipeWire
License:        MIT
Recommends:     %{name}%{?_isa} = %{version}-%{release}

%description gstreamer
This package contains GStreamer elements to interface with a
PipeWire media server.

%package devel
Summary:        Headers and libraries for PipeWire client development
License:        MIT
Requires:       %{name}-libs%{?_isa} = %{version}-%{release}
%description devel
Headers and libraries for developing applications that can communicate with
a PipeWire media server.

%package doc
Summary:        PipeWire media server documentation
License:        MIT

%description doc
This package contains documentation for the PipeWire media server.

%package utils
Summary:        PipeWire media server utilities
License:        MIT
Requires:       %{name}%{?_isa} = %{version}-%{release}
Requires:       %{name}-libs%{?_isa} = %{version}-%{release}

%description utils
This package contains command line utilities for the PipeWire media server.

%if 0%{?enable_alsa}
%package alsa
Summary:        PipeWire media server ALSA support
License:        MIT
Recommends:     %{name}%{?_isa} = %{version}-%{release}
Requires:       %{name}-libs%{?_isa} = %{version}-%{release}

%description alsa
This package contains an ALSA plugin for the PipeWire media server.
%endif

%if 0%{?enable_jack}
%package libjack
Summary:        PipeWire libjack library
License:        MIT
Recommends:     %{name}%{?_isa} = %{version}-%{release}
Requires:       %{name}-libs%{?_isa} = %{version}-%{release}
BuildRequires:  jack-audio-connection-kit-devel >= 1.9.10
# Renamed in F32
Obsoletes:      pipewire-jack < 0.2.96-2

%description libjack
This package contains a PipeWire replacement for JACK audio connection kit
"libjack" library.

%package jack-audio-connection-kit
Summary:        PipeWire JACK implementation
License:        MIT
Recommends:     %{name}%{?_isa} = %{version}-%{release}
Requires:       %{name}-libjack%{?_isa} = %{version}-%{release}
BuildRequires:  jack-audio-connection-kit-devel >= 1.9.10
Conflicts:      jack-audio-connection-kit
Conflicts:      jack-audio-connection-kit-dbus
Provides:       jack-audio-connection-kit

%description jack-audio-connection-kit
This package provides a JACK implementation based on PipeWire

%package plugin-jack
Summary:        PipeWire media server JACK support
License:        MIT
BuildRequires:  jack-audio-connection-kit-devel
Recommends:     %{name}%{?_isa} = %{version}-%{release}
Requires:       %{name}-libs%{?_isa} = %{version}-%{release}
Requires:       jack-audio-connection-kit

%description plugin-jack
This package contains the PipeWire spa plugin to connect to a JACK server.
%endif

%if 0%{?enable_pulse}
%package libpulse
Summary:        PipeWire libpulse library
License:        MIT
Recommends:     %{name}%{?_isa} = %{version}-%{release}
Requires:       %{name}-libs%{?_isa} = %{version}-%{release}
BuildRequires:  pulseaudio-libs-devel
# Renamed in F32
Obsoletes:      pipewire-pulseaudio < 0.2.96-2

%description libpulse
This package contains a PipeWire replacement for PulseAudio "libpulse" library.

%package pulseaudio
Summary:        PipeWire PulseAudio implementation
License:        MIT
Recommends:     %{name}%{?_isa} = %{version}-%{release}
Requires:       %{name}-libpulse%{?_isa} = %{version}-%{release}
BuildRequires:  pulseaudio-libs-devel
Conflicts:      pulseaudio-libs
Conflicts:      pulseaudio-libs-glib2
Provides:       pulseaudio-libs
Provides:       pulseaudio-libs-glib2

%description pulseaudio
This package provides a PulseAudio implementation based on PipeWire
%endif

%prep
%setup -q -T -b0 -n %{name}-%{version}%{?gitrel:-%{gitrel}-g%{shortcommit}}

%patch0 -p1 -b .0000

%build
%meson \
    -D docs=true -D man=true -D gstreamer=true -D systemd=true 		\
    %{!?enable_jack:-D jack=false -D pipewire-jack=false} 		\
    %{!?enable_pulse:-D pipewire-pulseaudio=false}			\
    %{!?enable_alsa:-D pipewire-alsa=false}				\
    %{!?enable_vulkan:-D vulkan=false}

%install
%meson_install

mkdir %{buildroot}%{_userunitdir}/sockets.target.wants
ln -s ../pipewire.socket %{buildroot}%{_userunitdir}/sockets.target.wants/pipewire.socket

%if 0%{?enable_jack}
ln -s pipewire-%{apiversion}/jack/libjack.so.0 %{buildroot}%{_libdir}/libjack.so.0.1.0
ln -s libjack.so.0.1.0 %{buildroot}%{_libdir}/libjack.so.0
ln -s pipewire-%{apiversion}/jack/libjackserver.so.0 %{buildroot}%{_libdir}/libjackserver.so.0.1.0
ln -s libjackserver.so.0.1.0 %{buildroot}%{_libdir}/libjackserver.so.0
ln -s pipewire-%{apiversion}/jack/libjacknet.so.0 %{buildroot}%{_libdir}/libjacknet.so.0.1.0
ln -s libjacknet.so.0.1.0 %{buildroot}%{_libdir}/libjacknet.so.0
%endif

%if 0%{?enable_pulse}
ln -s pipewire-%{apiversion}/pulse/libpulse.so.0 %{buildroot}%{_libdir}/libpulse.so.0
ln -s pipewire-%{apiversion}/pulse/libpulse-simple.so.0 %{buildroot}%{_libdir}/libpulse-simple.so.0
ln -s pipewire-%{apiversion}/pulse/libpulse-mainloop-glib.so.0 %{buildroot}%{_libdir}/libpulse-mainloop-glib.so.0
%endif

%if 0%{?enable_alsa}
mkdir -p %{buildroot}%{_sysconfdir}/alsa/conf.d/
cp %{buildroot}%{_datadir}/alsa/alsa.conf.d/50-pipewire.conf \
        %{buildroot}%{_sysconfdir}/alsa/conf.d/50-pipewire.conf
cp %{buildroot}%{_datadir}/alsa/alsa.conf.d/99-pipewire-default.conf \
        %{buildroot}%{_sysconfdir}/alsa/conf.d/99-pipewire-default.conf
%endif

%check
%ifarch s390x
# FIXME: s390x FAIL: pw-test-stream, pw-test-endpoint
%global tests_nonfatal 1
%endif
%meson_test || TESTS_ERROR=$?
if [ "${TESTS_ERROR}" != "" ]; then
echo "test failed"
%{!?tests_nonfatal:exit $TESTS_ERROR}
fi

%pre
getent group pipewire >/dev/null || groupadd -r pipewire
getent passwd pipewire >/dev/null || \
    useradd -r -g pipewire -d %{_localstatedir}/run/pipewire -s /sbin/nologin -c "PipeWire System Daemon" pipewire
exit 0

%ldconfig_scriptlets libs

%files
%license LICENSE COPYING
%doc README.md
%if 0%{?systemd}
%{_userunitdir}/pipewire.*
%{_userunitdir}/sockets.target.wants/pipewire.socket
%endif
%{_bindir}/pipewire
%{_bindir}/pipewire-media-session
%{_mandir}/man1/pipewire.1*
%dir %{_sysconfdir}/pipewire/
%config(noreplace) %{_sysconfdir}/pipewire/pipewire.conf
%{_mandir}/man5/pipewire.conf.5*

%files libs
%license LICENSE COPYING
%doc README.md
%{_libdir}/libpipewire-%{apiversion}.so.*
%{_libdir}/pipewire-%{apiversion}/libpipewire-*.so
%dir %{_libdir}/spa-%{spaversion}
%{_libdir}/spa-%{spaversion}/alsa/
%{_libdir}/spa-%{spaversion}/audioconvert/
%{_libdir}/spa-%{spaversion}/audiomixer/
%{_libdir}/spa-%{spaversion}/bluez5/
%{_libdir}/spa-%{spaversion}/control/
%{_libdir}/spa-%{spaversion}/support/
%{_libdir}/spa-%{spaversion}/v4l2/
%{_libdir}/spa-%{spaversion}/videoconvert/
%if 0%{?enable_vulkan}
%{_libdir}/spa-%{spaversion}/vulkan/
%endif

%files gstreamer
%{_libdir}/gstreamer-1.0/libgstpipewire.*

%files devel
%{_libdir}/libpipewire-%{apiversion}.so
%{_includedir}/pipewire-%{apiversion}/
%{_includedir}/spa-%{spaversion}/
%{_libdir}/pkgconfig/libpipewire-%{apiversion}.pc
%{_libdir}/pkgconfig/libspa-%{spaversion}.pc

%files doc
%{_datadir}/doc/pipewire/html

%files utils
%{_bindir}/pw-mon
%{_bindir}/pw-metadata
%{_bindir}/pw-mididump
%{_bindir}/pw-midiplay
%{_bindir}/pw-midirecord
%{_bindir}/pw-cli
%{_bindir}/pw-dot
%{_bindir}/pw-cat
%{_bindir}/pw-play
%{_bindir}/pw-profiler
%{_bindir}/pw-record
%{_mandir}/man1/pw-mon.1*
%{_mandir}/man1/pw-cli.1*
%{_mandir}/man1/pw-cat.1*
%{_mandir}/man1/pw-dot.1*
%{_mandir}/man1/pw-metadata.1*
%{_mandir}/man1/pw-mididump.1*
%{_mandir}/man1/pw-profiler.1*

%{_bindir}/spa-monitor
%{_bindir}/spa-inspect

%if 0%{?enable_alsa}
%files alsa
%{_libdir}/alsa-lib/libasound_module_pcm_pipewire.so
%{_datadir}/alsa/alsa.conf.d/50-pipewire.conf
%{_datadir}/alsa/alsa.conf.d/99-pipewire-default.conf
%config(noreplace) %{_sysconfdir}/alsa/conf.d/50-pipewire.conf
%config(noreplace) %{_sysconfdir}/alsa/conf.d/99-pipewire-default.conf
%endif

%if 0%{?enable_jack}
%files libjack
%{_libdir}/pipewire-%{apiversion}/jack/libjack.so*
%{_libdir}/pipewire-%{apiversion}/jack/libjacknet.so*
%{_libdir}/pipewire-%{apiversion}/jack/libjackserver.so*
%{_bindir}/pw-jack
%{_mandir}/man1/pw-jack.1*

%files jack-audio-connection-kit
%{_libdir}/libjack.so.*
%{_libdir}/libjackserver.so.*
%{_libdir}/libjacknet.so.*

%files plugin-jack
%{_libdir}/spa-%{spaversion}/jack/
%endif

%if 0%{?enable_pulse}
%files libpulse
%{_libdir}/pipewire-%{apiversion}/pulse/libpulse.so*
%{_libdir}/pipewire-%{apiversion}/pulse/libpulse-simple.so*
%{_libdir}/pipewire-%{apiversion}/pulse/libpulse-mainloop-glib.so*
%{_bindir}/pw-pulse
%{_mandir}/man1/pw-pulse.1*

%files pulseaudio
%{_libdir}/libpulse.so.0
%{_libdir}/libpulse-simple.so.0
%{_libdir}/libpulse-mainloop-glib.so.0
%endif

%changelog
* Tue Jun 23 2020 Wim Taymans <wtaymans@redhat.com> - 0.3.6-1
- Update to 0.3.6
- Resolves: rhbz#1832347

* Mon Jun 15 2020 Wim Taymans <wtaymans@redhat.com> - 0.3.5-3
- Rebuild
- Resolves: rhbz#1832347

* Tue May 19 2020 Wim Taymans <wtaymans@redhat.com> - 0.3.5-2
- Disable vulkan
- Resolves: rhbz#1832347

* Tue May 19 2020 Wim Taymans <wtaymans@redhat.com> - 0.3.5-1
- Update to 0.3.5
- Disable pulse and jack
- Add patch to work with meson 0.49
- Add patch to fix neon compilation
- Resolves: rhbz#1832347

* Fri Oct 18 2019 David King <dking@redhat.com> - 0.2.7-1
- Rebase to 0.2.7 (#1748331)

* Fri Jan 11 2019 Wim Taymans <wtaymans@redhat.com> - 0.2.5-1
- Update to 0.2.5
- Revert patch that requires too new meson
- Add patch to avoid invalid conversion error with C++ compilers
- Resolves: #1664569

* Fri Nov 30 2018 Wim Taymans <wtaymans@redhat.com> - 0.2.4-1
- Update to 0.2.4
- Add defines for cursor and bitmap metadata
- Revert patch that requires too new meson
- Resolves: #1655028

* Thu Oct 18 2018 Wim Taymans <wtaymans@redhat.com> - 0.2.3-2
- Add systemd socket activation
- Resolves: #1639871

* Fri Oct 12 2018 Wim Taymans <wtaymans@redhat.com> - 0.2.3-1
- Update to 0.2.3
- Resolves: #1638046

* Wed Aug 01 2018 Wim Taymans <wtaymans@redhat.com> - 0.2.2-1
- Update to 0.2.2

* Tue Feb 27 2018 Wim Taymans <wtaymans@redhat.com> - 0.1.9-1
- Update to 0.1.9

* Fri Feb 09 2018 Fedora Release Engineering <releng@fedoraproject.org> - 0.1.8-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Sat Feb 03 2018 Igor Gnatenko <ignatenkobrain@fedoraproject.org> - 0.1.8-2
- Switch to %%ldconfig_scriptlets

* Tue Jan 23 2018 Wim Taymans <wtaymans@redhat.com> - 0.1.8-1
- Update to 0.1.8

* Fri Nov 24 2017 Wim Taymans <wtaymans@redhat.com> - 0.1.7-1
- Update to 0.1.7
- Add to build when memfd_create is already defined

* Fri Nov 03 2017 Wim Taymans <wtaymans@redhat.com> - 0.1.6-1
- Update to 0.1.6

* Tue Sep 19 2017 Wim Taymans <wtaymans@redhat.com> - 0.1.5-2
- Add patch to avoid segfault when probing

* Tue Sep 19 2017 Wim Taymans <wtaymans@redhat.com> - 0.1.5-1
- Update to 0.1.5

* Thu Sep 14 2017 Kalev Lember <klember@redhat.com> - 0.1.4-3
- Rebuilt for GNOME 3.26.0 megaupdate

* Fri Sep 08 2017 Wim Taymans <wtaymans@redhat.com> - 0.1.4-2
- Install SPA hooks

* Wed Aug 23 2017 Wim Taymans <wtaymans@redhat.com> - 0.1.4-1
- Update to 0.1.4

* Wed Aug 09 2017 Wim Taymans <wtaymans@redhat.com> - 0.1.3-1
- Update to 0.1.3

* Tue Jul 04 2017 Wim Taymans <wtaymans@redhat.com> - 0.1.2-1
- Update to 0.1.2
- Added more build requirements
- Make separate doc package

* Mon Jun 26 2017 Wim Taymans <wtaymans@redhat.com> - 0.1.1-1
- Update to 0.1.1
- Add dbus-1 to BuildRequires
- change libs-devel to -devel

* Wed Sep 9 2015 Wim Taymans <wtaymans@redhat.com> - 0.1.0-2
- Fix BuildRequires to use pkgconfig, add all dependencies found in configure.ac
- Add user and groups  if needed
- Add license to %%licence

* Tue Sep 1 2015 Wim Taymans <wtaymans@redhat.com> - 0.1.0-1
- First version
